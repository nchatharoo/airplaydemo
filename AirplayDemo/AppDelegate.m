//
//  AppDelegate.m
//  AirplayDemo
//
//  Created by Nadheer Chatharoo on 16/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
- (UIWindow *)  createWindowForScreen:(UIScreen *)screen;
- (void)        addViewController:(UIViewController *)controller toWindow:(UIWindow *)window;
- (void)        screenDidConnect:(NSNotification *) notification;
- (void)        screenDidDisconnect:(NSNotification *) notification;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    UIWindow    *_window    = nil;
//    NSArray     *_screens   = nil;
    
    _arrayOfWindows = [[NSMutableArray alloc] init];
    
    NSArray *_screens = [UIScreen screens];
    for (UIScreen *_screen in _screens){
        
        UIWindow *_window = [self createWindowForScreen:_screen];

        // If you don't do this here, you will get the "Applications are expected to have a root view controller" message.
        if (_screen == [UIScreen mainScreen]){
            
            self.windowM = _window;
            self.windowM.backgroundColor = [UIColor darkGrayColor];
            _viewController = [[AirplayDemoViewController alloc] init];
            self.windowM.rootViewController = _viewController;
            [self.windowM makeKeyAndVisible];
        }  else {
            _screen.overscanCompensation = UIScreenOverscanCompensationInsetApplicationFrame;
            UIViewController *tempController = [[UIViewController alloc] init];
            [_window setRootViewController:tempController];
            [_window setHidden:NO];
        }
        
    }
    
    // Register for notification
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidConnect:)
												 name:UIScreenDidConnectNotification
											   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(screenDidDisconnect:)
												 name:UIScreenDidDisconnectNotification
											   object:nil];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidConnectNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIScreenDidDisconnectNotification object:nil];

}

- (UIWindow *) createWindowForScreen:(UIScreen *)screen {
    UIWindow    *_window;

    for (UIWindow *tempWindow in _arrayOfWindows){
        if (tempWindow.screen == screen){
            _window = tempWindow;
        }
    }

    if (_window == nil){
        _window = [[UIWindow alloc] initWithFrame:[screen bounds]];
        [_window setScreen:screen];
        [_arrayOfWindows addObject:_window];
    }
    
    return _window;
}

- (void) addViewController:(UIViewController *)controller toWindow:(UIWindow *)window {
    [window setRootViewController:controller];
    [window setHidden:NO];
}

- (void) screenDidConnect:(NSNotification *) notification {
    UIScreen *connectedScreen = [notification object];
    connectedScreen.overscanCompensation = UIScreenOverscanCompensationInsetApplicationFrame;
    UIWindow *tempWindow = [self createWindowForScreen:connectedScreen];
    UIViewController *tempController = [[UIViewController alloc] init];
    [tempWindow setRootViewController:tempController];
    [tempWindow setHidden:NO];
}

- (void) screenDidDisconnect:(NSNotification *) notification {
    UIScreen *disconnectedScreen = [notification object];
    for (UIWindow *tempWindow in _arrayOfWindows){
        if (tempWindow.screen == disconnectedScreen){
            NSUInteger windowIndex = [_arrayOfWindows indexOfObject:tempWindow];
            [_arrayOfWindows removeObjectAtIndex:windowIndex];
        }
    }
    return;
}


@end
