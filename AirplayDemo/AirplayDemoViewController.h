//
//  AirplayDemoViewController.h
//  AirplayDemo
//
//  Created by Nadheer Chatharoo on 16/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class AVPlayer, AVPlayerLayer;

@interface AirplayDemoViewController : UIViewController {
    UILabel *textLabel;
    
    AVPlayer *player;
    AVPlayerLayer *playerLayer;
}

@end
