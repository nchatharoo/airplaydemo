//
//  ViewController.m
//  AirplayDemo
//
//  Created by Nadheer Chatharoo on 16/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
-(NSUInteger) screenNumber;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    textLabel.textColor = [UIColor redColor];
    textLabel.backgroundColor = [UIColor blackColor];
    NSUInteger screenNumber = [self screenNumber];
    [textLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)screenNumber]];
    
    [self.view addSubview:textLabel];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger) screenNumber {
    NSUInteger  result      = 1;
    UIWindow    *_window    = nil;
    UIScreen    *_screen    = nil;
    NSArray     *_screens   = nil;
    
    _screens = [UIScreen screens];
    
    if ([_screens count] > 1){
        _window = [[self view] window];
        _screen = [_window screen];
        if (_screen != nil){
            for(size_t i = 0; i < [_screens count]; ++i){
                UIScreen *_currentScreen = [_screens objectAtIndex:i];
                if (_currentScreen == _screen){
                    result = i+1;
                }
            }
        }
    }
    return result;
}


@end
