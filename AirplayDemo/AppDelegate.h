//
//  AppDelegate.h
//  AirplayDemo
//
//  Created by Nadheer Chatharoo on 16/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AirplayDemoViewController.h"

@class AirplayDemoViewController;
@interface AppDelegate : NSObject <UIApplicationDelegate>

@property (strong, nonatomic) NSMutableArray *arrayOfWindows;
@property (strong, nonatomic) AirplayDemoViewController *viewController;
@property (strong, nonatomic) UIWindow *windowM;
@end
