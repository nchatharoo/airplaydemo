//
//  AirplayDemoViewController.m
//  AirplayDemo
//
//  Created by Nadheer Chatharoo on 16/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//
#import "AppDelegate.h"
#import "AirplayDemoViewController.h"

@interface AirplayDemoViewController ()
-(NSUInteger) screenNumber;
@end

@implementation AirplayDemoViewController {
    NSUInteger screenNumber;
    UIWindow *secondWindow;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    screenNumber = [self screenNumber];
//    NSLog(@"%lu", (unsigned long)screenNumber);
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    self.view.backgroundColor = [UIColor whiteColor];
    
//    UIScreen * secondScreen = [[UIScreen screens] objectAtIndex:1];
//    NSLog(@"%lu", (unsigned long)screenNumber);
//    CGRect screenBounds = secondScreen.bounds;

    UIImage *image = [UIImage imageNamed:@"cosmoSmall.jpg"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 427, 427);
    [btn setImage:image forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(initThePlayer) forControlEvents:UIControlEventTouchUpInside];

    NSLog(@"%@ 1er log de btn", btn);
    [self.view addSubview:btn];

}

-(void)initThePlayer {

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.arrayOfWindows count] > 1) {
        
        secondWindow = appDelegate.arrayOfWindows[1];
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"testVid" ofType:@"mp4"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        player = [[AVPlayer alloc] initWithURL:fileURL];
        
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
        playerLayer.frame = secondWindow.frame;
        NSLog(@"%@ NSStringFromCGRect(secondWindow.frame)", NSStringFromCGRect(playerLayer.frame));
        [secondWindow.window.layer addSublayer:playerLayer];
        
        [player play];
    } else {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        label.text = @"NO VIDEO";
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:label];
    }
}

/*-(void)displayBigPic {
    NSLog(@"ici");
    if (screenNumber == 2) {
        UIScreen * secondScreen = [[UIScreen screens] objectAtIndex:1];
        NSLog(@"%lu", (unsigned long)screenNumber);
        CGRect screenBounds = secondScreen.bounds;
        
        secondWindow = [[UIWindow alloc] initWithFrame:screenBounds];
        secondWindow.screen = secondScreen;
        secondWindow.hidden = NO;
//        [secondWindow.window.layer addSublayer:playerLayer];
//        [player play];
        
    }
//    UIImage *image = [UIImage imageNamed:@"cosmoBig.png"];
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    imageView.image = image;
//    [self.view addSubview:imageView];
    
//    [secondWindow.window addSubview:imageView];
    
//    [[UIScreen mainScreen].mirroredScreen bounds];
}*/



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger) screenNumber {
    NSUInteger  result      = 1;
    UIWindow    *_window    = nil;
    UIScreen    *_screen    = nil;
    NSArray     *_screens   = nil;
    
    _screens = [UIScreen screens];
    
    if ([_screens count] > 1){
        _window = [[self view] window];
        _screen = [_window screen];
        if (_screen != nil){
            for(size_t i = 0; i < [_screens count]; ++i){
                UIScreen *_currentScreen = [_screens objectAtIndex:i];
                if (_currentScreen == _screen){
                    result = i+1;
                }
            }
        }
    }
    return result;
    NSLog(@"%lu", (unsigned long)result);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
